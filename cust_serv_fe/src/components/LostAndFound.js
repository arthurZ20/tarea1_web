import React, {useState} from 'react';

import { Button, Input, InputLabel, TableHead, 
         TableRow, TableCell, TableBody, Divider } from '@material-ui/core';
import moment from 'moment';

function LostAndFound() {
  const [trips, setTrips] = useState([]);
  const [date, setDate] = useState(null);
  const [address, setAddress] = useState(null);

  const fetchTrips = async () =>{
    const formattedDate = moment.utc(date).format();
    const res = await fetch(`http://localhost:3000/lost_and_found?date=${date?formattedDate:''}&address=${address?address:''}}`);

    const trips = await res.json();
    setTrips(trips);
  };

  return (
    <div>
      <form>
        <InputLabel htmlFor="date-of-trip">Incident date</InputLabel>
        <Input id="date-of-trip" type="date" onChange={(sd) => setDate(sd.target.value)}/>
        <p/>
        <InputLabel htmlFor="pick-up-address">Pick up address</InputLabel>
        <Input id="pick-up-address" fullWidth onChange={(sa) => setAddress(sa.target.value)}/>
        <p/>
        <Button id="submit-button" variant="outlined" color="primary" onClick={fetchTrips}>Submit</Button>
      </form>
      <p/>
      <Divider/>
      <table style={{width: "100%"}}>
        <TableHead>
          <TableRow>
            <TableCell>Date</TableCell>
            <TableCell>Pick up time</TableCell>
            <TableCell>Pick up address</TableCell>
            <TableCell>Drop off address</TableCell>
            <TableCell>Taxi ID</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {trips.map((trip, i) => 
          <TableRow key={i} className="data-row">
            <TableCell>{moment.utc(trip.date_of_trip).format('YY-MM-DD')}</TableCell>
            <TableCell>{moment.utc(trip.pickup_time).format('HH:mm:ss')}</TableCell>
            <TableCell>{trip.pickup_address}</TableCell>
            <TableCell>{trip.dropoff_address}</TableCell>
            <TableCell>{trip.taxi_id}</TableCell>
          </TableRow>
          )}
        </TableBody>
      </table>
    </div>
  );
}

export default LostAndFound;
