import React, {useState} from 'react';
import moment from 'moment';

import { TableHead, 
         TableRow, TableCell, TableBody, TextField, FormControlLabel, Checkbox, Grid, Button, ButtonGroup, Divider } from '@material-ui/core';

function ScamAnalysis() {
  const [trips, setTrips] = useState([]);
  const [date, setDate] = useState(null);
  const [address, setAddress] = useState(null);
  const [dropaddress, setDropAddress] = useState(null);
  const [taxi, setTaxi] = useState(null);
  const [checked, setChecked] = useState(null);
  const [checked50, setChecked50] = useState(null);

  const fetchTrips = async () =>{
    const formattedDate = moment.utc(date).format();
    const res = await fetch(`http://localhost:3000/scam_analysis?date=${date?formattedDate:""}&address=${address?address:""}&dropaddress=${dropaddress?dropaddress:""}&taxi=${taxi?taxi:""}`);

    const trips = await res.json();
    for (let i = 0; i < trips.length; i++) {
      let fecha1 = new moment.utc(trips[i].pickup_time);
      let fecha2 = new moment.utc(trips[i].dropoff_time);
      let resta = moment.duration(fecha2.diff(fecha1).get('minutes'));
      let result = ((resta/moment.utc(trips[i].est_duration).format('mm'))-1)*100;
      trips[i].color={result};      
    }
    setTrips(trips);
    // console.log(trips);
  };

  const clear = () => {
    document.getElementById("incident-date").value='';
    document.getElementById("pick-up-address").value='';
    document.getElementById("drop-off-address").value='';
    document.getElementById("taxi-id").value='';
    setTrips([]);
    setDate(null);
    setAddress(null);
    setDropAddress(null);
    setTaxi(null);
    setChecked(false);
    setChecked50(false);
  } 

  return (
    <div>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <TextField id="incident-date" label="Incident date" type="date" InputLabelProps={{shrink: true}} onChange={e => setDate(e.target.value)}/>
          <TextField id="pick-up-address" label="Pick up address" fullWidth onChange={e => setAddress(e.target.value)}/>
          <TextField id="drop-off-address" label="Drop off address" fullWidth onChange={e => setDropAddress(e.target.value)}/>
          <TextField id="taxi-id" label="Taxi ID" onChange={e => setTaxi(e.target.value) }/>
        </Grid>
        <Grid item xs={6}>
          <FormControlLabel 
            control={<Checkbox id="overpriced25" checked= {checked} onClick={e=> {setChecked(e.target.checked)}} />} 
            label="Overpriced trips >= 25%" />
          <FormControlLabel 
            control={<Checkbox id="overpriced50" checked= {checked50} onClick={e=> {setChecked50(e.target.checked)}} />} 
            label="Overpriced trips >= 50%" />
          <p/>
          <ButtonGroup orientation="vertical" style={{width: "50%"}}>
            <Button id="submit-button" variant="outlined" color="primary" onClick= {fetchTrips}>Submit</Button>
            <Button id="clear-form-button" variant="outlined" color="primary" onClick={clear}>Clear form</Button>
          </ButtonGroup>
        </Grid>
      </Grid>
      <p/>
      <Divider/>

      <table style={{width: "100%"}}>
        <TableHead>
          <TableRow>
            <TableCell>Date</TableCell>
            <TableCell>Pick up time</TableCell>
            <TableCell>Drop off time</TableCell>
            <TableCell>Est pick up time</TableCell>
            <TableCell>Est duration</TableCell>
            <TableCell>Pick up address</TableCell>
            <TableCell>Drop off address</TableCell>
            <TableCell>Taxi ID</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {trips.map((trip,i) => 
            <TableRow className="data-row" key={i} style={(checked===true&&trip.color.result>=25&&checked50===false)?{backgroundColor: '#FFFF00'}:(checked===true&&trip.color.result>=25&&checked50===true&&trip.color.result<50)?{backgroundColor: '#FFFF00'}:(checked===true&&trip.color.result>=50&&checked50===true)?{backgroundColor: '#FF0000'}:(checked===false&&trip.color.result>=50&&checked50===true)?{backgroundColor: '#FF0000'}:{}}>
            <TableCell>{moment.utc(trip.date_of_trip).format('YYYY-MM-DD')}</TableCell>
            <TableCell>{moment.utc(trip.pickup_time).format('HH:mm:ss')}</TableCell>
            <TableCell>{moment.utc(trip.dropoff_time).format('HH:mm:ss')}</TableCell>
            <TableCell>{moment.utc(trip.est_pickup_time).format('HH:mm:ss')}</TableCell>
            <TableCell>{moment.utc(trip.est_duration).format('HH:mm:ss')}</TableCell>
            <TableCell>{trip.pickup_address}</TableCell>
            <TableCell>{trip.dropoff_address}</TableCell>
            <TableCell>{trip.taxi_id}</TableCell>
          </TableRow>
          )}
        </TableBody>
      </table>
    </div>
  );
}

export default ScamAnalysis;
