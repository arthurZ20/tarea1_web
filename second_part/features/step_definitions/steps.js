const {Given, When, Then, After, Before} = require('cucumber');
const puppeteer = require('puppeteer');
const mongoose = require('mongoose');
const moment = require("moment");
const assert = require('assert');

var browser;
var page;

const Trips = mongoose.model("Trips2",
  new mongoose.Schema({
    date_of_trip: Date,
    pickup_time: Date,
    dropoff_time: Date,
    est_pickup_time:Date,
    est_duration:Date,
    pickup_address: String,
    dropoff_address: String,
    taxi_id: Number,
  })
);

// TODO: Replace the name of your database with the one you are using for the backend
Before(async function() {
  mongoose.connect('mongodb://localhost/custom_serv', {useNewUrlParser: true});
  browser = await puppeteer.launch({headless: false});
});

After(async function() {
  // await browser.close();
  await Trips.deleteMany({});
  await mongoose.connection.close();
});

//STRS's ride history includes the following trips
Given('STRS\'s ride history includes the following trips', async function (dataTable) {
  const trips = dataTable.hashes().map((trip) => {
    // console.log(moment(trip.date).format());
    return {
        date_of_trip: moment.utc(trip.date).format(),
        pickup_time: moment.utc(`${trip.date} ${trip.pickup_time}`).format(),
        dropoff_time: moment.utc(`${trip.date} ${trip.dropoff_time}`).format(),
        est_pickup_time: moment.utc(`${trip.date} ${trip.est_pickup_time}`).format(),
        est_duration:moment.utc(`${trip.date} ${trip.est_duration}`).format(),
        pickup_address: trip.pickup_address,
        dropoff_address: trip.dropoff_address,
        taxi_id: trip.taxi_id
    };
  });
  await Trips.insertMany(trips);
  page = await browser.newPage();
  await page.goto('http://localhost:3001/scam_analysis');
});

//"John Smith" lodges a customer service request for abusive behavior
Given('{string} lodges a customer service request for abusive behavior', function (string) {
});

//the date of the trip is "2020-03-16"
Given('the date of the trip is {string}', async function (date_of_trip) {
  await page.focus('#incident-date');
  await page.keyboard.type(moment(date_of_trip).format('DD/MM/YYYY'));
});

//the taxi ride was from "Benito Juarez 51-B" to "Av 25 Poniente 1301"
Given('The taxi ride was from {string} to {string}', async function (pickup_address,dropoff_address) {
    await page.focus('#pick-up-address');
    await page.keyboard.type(pickup_address);
    await page.focus('#drop-off-address');
    await page.keyboard.type(dropoff_address);
});

// the operator enters the date, pickup and drop off addresses of the trip
When('the operator enters the date, pickup and drop off addresses of the trip', async function () {
  await page.focus('#submit-button'); 
  await page.keyboard.press('Enter', {delay: 1000});
});

Then('the operator should see {int} row associated with taxi {int}', function (int,taxi_id) {(async() => { 
    let number_of_rows = await page.$eval('table tbody tr', rows => rows.length);
    assert.equal(number_of_rows,int);
    await Trips.find(dataTable.hashes());
    assert(await page.evaluate(() => window.find(taxi_id)));
  }); 
});

//the operator clears the form
When('the operator clears the form', async function () {
    await page.focus('#clear-form-button'); 
    await page.keyboard.press('Enter', {delay: 1000});
});

//the operator enters the taxi identifier 3
When('the operator enters the taxi identifier {int}', async function (int) {
    const n =int.toString();
    await page.focus('#taxi_id'); 
    await page.keyboard.type(n);
    await page.focus('#submit-button'); 
    await page.keyboard.press('Enter', {delay: 1000});
});

Then('the operator should see {int} rows', function (int) {(async() => {
    let number_of_rows = await page.$eval('table tbody tr', rows => rows.length);
    assert.equal(number_of_rows,int);
    });
});

//the operator filters 25% or more overpriced trips
//the operator filters 50% or more overpriced trips
When('the operator filters {int}% or more overpriced trips', async function (int) {
  await page.focus(`#overpriced${int}`);
  await page.click(`#overpriced${int}`);
  await page.focus('#taxi_id');
  await page.keyboard.type('3');
  await page.focus('#submit-button');
  await page.keyboard.press('Enter', {delay: 1000});
});

Then('the operator should see {int} yellow row(s)', function (int) {(async() => {
  const counter = await page.$eval('table tbody tr', rows => {
    let counter = 0;
    rows.forEach((row) => {
      if (getComputedStyle(row).getPropertyValue("background-color")='#FFFF00') {
        counter ++;        
      }      
    });
    return counter;
  });
    assert.equal(counter,int);
  });
});

Then('the operator should see {int} red row(s)', function (int) {(async() => {
  const counter = await page.$eval('table tbody tr', rows => {
    let counter = 0;
    rows.forEach((row) => {
      if (getComputedStyle(row).getPropertyValue("background-color")='#FF0000') {
        counter ++;        
      }      
    });
    return counter;
  });
    assert.equal(counter,int);
  });
});
