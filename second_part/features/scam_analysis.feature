Feature: Identification of driver scams
  As an operator of STRS's customer service
  Such that I can investigate possible scam driver
  I want to get access to the taxi ride statistics based on a customer report

  Scenario: Identifying taxi based on report and highlighing overpriced trips
    Given STRS's ride history includes the following trips
        | date       | pickup_time | dropoff_time | est_pickup_time | est_duration | pickup_address         | dropoff_address           | taxi_id |
        | 2020-03-16 | 10:28:30    | 10:44:16     | 10:30:00        | 00:11:00     | Via Atlixcayotl 5718   | Blvd Atlixco 3304         | 3       |
        | 2020-03-16 | 10:42:17    | 10:51:40     | 10:42:00        | 00:09:00     | Av del Sol 5           | Av Kepler 2143            | 2       | 
        | 2020-03-16 | 11:31:42    | 11:45:00     | 11:32:00        | 00:13:00     | Via Atlixcayotl 5718   | Osa Mayor 2902            | 1       |
        | 2020-03-16 | 12:11:20    | 12:21:10     | 12:11:00        | 00:10:00     | Via Atlixcayotl 5718   | Cto Juan Pablo II 1920    | 4       |
        | 2020-03-16 | 15:10:10    | 15:30:20     | 15:12:00        | 00:12:00     | Benito Juarez 51-B     | Av 25 Poniente 1301       | 3       |
        | 2020-03-16 | 16:16:10    | 16:34:22     | 16:20:00        | 00:12:00     | Blvd Nino Poblano 2901 | Cto Juan Pablo II 3515    | 3       |
        | 2020-03-16 | 18:30:44    | 18:44:00     | 18:30:00        | 00:15:00     | C Real Sn Andres 4002  | Blvd Municipio Libre 1923 | 1       |
    And "John Smith" lodges a customer service request for abusive behavior
    And the date of the trip is "2020-03-16"
    And the taxi ride was from "Benito Juarez 51-B" to "Av 25 Poniente 1301"
    When the operator enters the date, pickup and drop off addresses of the trip
    Then the operator should see 1 row associated with taxi 3
    When the operator clears the form
    And the operator enters the taxi identifier 3
    Then the operator should see 3 rows

    When the operator filters 25% or more overpriced trips
    Then the operator should see 3 yellow rows
    When the operator filters 50% or more overpriced trips
    Then the operator should see 1 yellow row
    Then the operator should see 2 red rows
