const {Given, When, Then, After, Before} = require('cucumber');
const puppeteer = require('puppeteer');
const mongoose = require('mongoose');
const moment = require("moment");

var browser;
var page;
var date = null;

const Trips = mongoose.model("Trips",
  new mongoose.Schema({
    date_of_trip: Date,
    pickup_time: Date,
    pickup_address: String,
    dropoff_address: String,
    taxi_id: Number,
  })
);

// TODO: Replace the name of your database with the one you are using for the backend
Before(async function() {
  mongoose.connect('mongodb://localhost/custom_serv', {useNewUrlParser: true});
  browser = await puppeteer.launch({headless: false});
});

After(async function() {
  // await browser.close();
  Trips.deleteMany({});
  await mongoose.connection.close();
});

//STRS's ride history includes the following trips
Given('STRS\'s ride history includes the following trips', async function (dataTable) {
  const trips = dataTable.hashes().map((trip) => {
    // console.log(moment(trip.date).format());
    return {
      date_of_trip: moment.utc(trip.date).format(),
      pickup_time: moment(`${trip.date} ${trip.pickup_time}`).format(),
      pickup_address: trip.pickup_address,
      dropoff_address: trip.dropoff_address,
      taxi_id: trip.taxi_id
    };
  });
  await Trips.insertMany(trips);
  page = await browser.newPage();
  await page.goto('http://localhost:3000/lost_and_foundejs');
});

//"John Smith" lodges a customer service request for lost object
Given('{string} lodges a customer service request for lost object', function (string) {
});

//the lost object is a "smart phone"
Given('the lost object is a {string}', function (string) {
});

//the date of the loss is "2020-03-16"
Given('the date of the loss is {string}', async function (date_of_trip) {
  // await page.$eval('#date-of-trip', (el, value) => el.value = value, date_of_trip);
  await page.focus('#date-of-trip');
  await page.keyboard.type(moment(date_of_trip, 'YYYY-MM-DD').format('DD/MM/YYYY'));
});

//the operator enters the date of loss
When('the operator enters the date of loss', async function () {
  await page.focus('#submit-button'); 
  // await page.keyboard.press('Enter', {delay: 1000});
});

//the operator enters "Via Atlixcayotl 5718" as pickup address
When('the operator enters {string} as pickup address', async function (string) {
  await page.focus("#date-of-trip");
  await page.keyboard.type(moment(date, 'YYYY-MM-DD').format('DD/MM/YYYY'));
  await page.focus('#pick-up-address');
  await page.keyboard.type(string);

  await page.focus('#submit-button');
  await page.keyboard.press('Enter', {delay: 1000});  
});

//Then the operator should see 2 rows
Then('the operator should see {int} rows', function (int) {(async() => { 
  let number_of_rows = await page.$eval('table tbody tr', rows => rows.length);
    assert.equal(number_of_rows,int);
  }); 
});