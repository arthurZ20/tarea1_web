var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');

var indexRouter = require('./routes/index');
var lostAndFoundRouter = require('./routes/lost_and_found');
var scamAnalysisRouter = require('./routes/scam_analysis');
var lostAndFoundejsRouter = require('./routes/lost_and_foundejs');
var scamAnalysisejsRouter = require('./routes/scam_analysisejs');
var app = express();
app.use(cors());


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static('node_modules'));
// app.use(express.bodyParser());

app.use('/', indexRouter);
app.use('/lost_and_found', lostAndFoundRouter);
app.use('/scam_analysis', scamAnalysisRouter);
app.use('/lost_and_foundejs', lostAndFoundejsRouter);
app.use('/scam_analysisejs', scamAnalysisejsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
