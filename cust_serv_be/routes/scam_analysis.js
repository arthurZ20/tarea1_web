var express = require('express');
var router = express.Router();
const {models} = require("../models");

/* GET users listing. */
router.get('/', async function(req, res, next) {
  try{
      
    const fil = {};
    req.query.date ? (fil.date_of_trip = new Date(req.query.date).toISOString()) : "";
    req.query.address ? (fil.pickup_address = req.query.address) : "";
    req.query.dropaddress ? (fil.dropoff_address=req.query.dropaddress): "";
    req.query.taxi ? (fil.taxi_id=req.query.taxi): "";
    const trips = await models.Trip2.find(fil);
    res.send(JSON.stringify(trips));
    
  }
  catch(error){
    console.log(error);
  }
});

module.exports = router;
