var express = require('express');
var router = express.Router();
var moment = require('moment');
var {models} = require("../models/");
var fil = {};

/* GET all movies. */
router.get('/', function(req, res, next) {
  console.log(req.body);
  models.Trip.find(fil).then(trips =>
    res.render('lost_and_found/index', {trips: trips, moment: moment, fil:fil})
    );
});

router.post('/', function (req, res, next) {
  fil = {};
  try{
    req.body.date_of_trip ? (fil.date_of_trip = new Date(req.body.date_of_trip).toISOString()) : "";
    req.body.pickup_address ? (fil.pickup_address = req.body.pickup_address) : "";
    models.Trip.find({fil});
    res.redirect('/lost_and_foundejs/');
    console.log(fil);
  }
  catch(error){
    console.log(error);
  }
});

module.exports = router;