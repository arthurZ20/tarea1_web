var express = require('express');
var router = express.Router();
var {models} = require("../models")

/* GET users listing. */
router.get('/', async function(req, res, next) {
  try{
      
    const fil = {};
    req.query.date ? (fil.date_of_trip = new Date(req.query.date).toISOString()) : "";
    req.query.address ? (fil.pickup_address = req.query.address) : "";
    
    const trips = await models.Trip.find(fil);
    res.send(JSON.stringify(trips));
    
  }
  catch(error){
    console.log(error);
  }
}); 

module.exports = router;
