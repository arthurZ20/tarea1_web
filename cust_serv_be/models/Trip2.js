'use strict';
module.exports = mongoose => {
  const newSchema = new mongoose.Schema({
    date_of_trip: {
      type: Date
    },
    pickup_time: {
      type: Date
    },
    dropoff_time: {
      type: Date
    },
    est_pickup_time: {
      type: Date
    },
    est_duration: {
      type: Date
    },
    pickup_address: {
      type: String
    },
    dropoff_address: {
      type: String
    },
    taxi_id: {
      type: Number
    }
  }, {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  });
  const Trip2 = mongoose.model('Trip2', newSchema);
  return Trip2;
};